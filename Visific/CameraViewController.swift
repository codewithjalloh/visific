//
//  CameraViewController.swift
//  Visific
//
//  Created by wealthyjalloh on 09/09/2017.
//  Copyright © 2017 CWJ. All rights reserved.
//

import UIKit
import AVFoundation
import CoreML
import Vision


enum FlashState {
    case off
    case on
}



class CameraViewController: UIViewController {
    
    
    // MARK: Global Variables
    var captureSession: AVCaptureSession!
    var cameraOutput: AVCapturePhotoOutput!
    var previewLayer: AVCaptureVideoPreviewLayer!
    
    
    var photoData: Data?
    var flashControllerState: FlashState = .off
    var speechSynthesizer = AVSpeechSynthesizer()
    
    
    
    // MARK: Outlets
    
    
    
    @IBOutlet weak var captureImageView: RoundedShadowImageview!
    @IBOutlet weak var flashButton: RoundedShadowButton!
    @IBOutlet weak var identificationLabel: UILabel!
    @IBOutlet weak var confidenceLabel: UILabel!
    @IBOutlet weak var roundedLabelView: RoundedShadow!
    @IBOutlet weak var cameraView: UIView!
    @IBOutlet weak var spinner: UIActivityIndicatorView!
    
    
    
    // MARK: Views Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        previewLayer.frame = cameraView.bounds
        speechSynthesizer.delegate = self
        spinner.isHidden = true
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        // tap gesture target
        let tap = UITapGestureRecognizer(target: self, action: #selector(CameraViewController.didTapCamView))
        
        // number tap required
        tap.numberOfTapsRequired = 1
        
        captureSession = AVCaptureSession()
        captureSession.sessionPreset = .hd1920x1080
        
        
        let backCamera = AVCaptureDevice.default(for: .video)
        
        do {
            let input = try AVCaptureDeviceInput(device: backCamera!)
            if captureSession.canAddInput(input) == true {
                captureSession.addInput(input)
            }
            
            
            cameraOutput = AVCapturePhotoOutput()
            if captureSession.canAddOutput(cameraOutput) == true {
                captureSession.addOutput(cameraOutput!)
                
                previewLayer = AVCaptureVideoPreviewLayer(session: captureSession!)
                previewLayer.videoGravity = AVLayerVideoGravity.resizeAspect
                previewLayer.connection?.videoOrientation = .portrait
                
                cameraView.addGestureRecognizer(tap)
                cameraView.layer.addSublayer(previewLayer!)
                captureSession.startRunning()
            }
            
        } catch {
            debugPrint(error)
        }
        
    }
    
    
    // MARK: Functions
    
    @objc func didTapCamView() {
        let settings = AVCapturePhotoSettings()
        let previewPixelType = settings.availablePreviewPhotoPixelFormatTypes.first!
        
        let previewFormat = [kCVPixelBufferPixelFormatTypeKey as String: previewPixelType, kCVPixelBufferWidthKey as String: 160, kCVPixelBufferHeightKey as String: 160]
        
        settings.previewPhotoFormat = previewFormat
        
        
        if flashControllerState == .off {
            settings.flashMode = .off
        } else {
            settings.flashMode = .on
        }
        
        
        cameraOutput.capturePhoto(with: settings, delegate: self)
        
    }
    
    
    
    
    
    // MARK: CoreML
    func resultMethod(request: VNRequest, error: Error?) {
        
        guard let results = request.results as? [VNClassificationObservation] else {
            return
        }
        
        
        for classification in results {
            print(classification.identifier)
            
            if classification.confidence < 0.5 {
                
                let unknownObjectMessage = "I am not sure what this is. Please try again"
                
                self.identificationLabel.text = unknownObjectMessage
                synthesizeSpeech(fromString: unknownObjectMessage)
                
                self.confidenceLabel.text = ""
                break
            } else {
                let identification = classification.identifier
                let confidence = Int(classification.confidence * 100)
                self.identificationLabel.text = identification
                
                
                self.confidenceLabel.text = "CONFIDENCE: \(confidence)% "
                
                let complateSentence = "This is look like a \(identification) and I'm \(confidence) percent sure."
                
                synthesizeSpeech(fromString: complateSentence)
                break
            }
        }
    }
    
    
    func synthesizeSpeech(fromString string: String) {
        let speechUtterance = AVSpeechUtterance(string: string)
        speechSynthesizer.speak(speechUtterance)
    }
    
    
    
    @IBAction func flashButtonPressed(_ sender: Any) {
        switch flashControllerState {
        case .off:
            flashButton.setTitle("FLASH ON", for: .normal)
            flashControllerState = .on
            
        case .on:
            flashButton.setTitle("FLASH OFF", for: .normal)
            flashControllerState = .off
        }
    }

}


// MARK: Extension - Camera Capture & Speech Synth

extension CameraViewController: AVCapturePhotoCaptureDelegate {
    
    func photoOutput(_ output: AVCapturePhotoOutput, didFinishProcessingPhoto photo: AVCapturePhoto, error: Error?) {
        
        if let error = error {
            debugPrint(error)
            
        } else {
            photoData = photo.fileDataRepresentation()
            
            do {
                let model = try VNCoreMLModel(for: SqueezeNet().model)
                let request = VNCoreMLRequest(model: model, completionHandler: resultMethod )
                
                let handler = VNImageRequestHandler(data: photoData!)
                try handler.perform([request])
            } catch {
                // handle error
                debugPrint(error)
            }
            
            let image = UIImage(data: photoData!)
            self.captureImageView.image = image
        }
    }
    
}



extension CameraViewController: AVSpeechSynthesizerDelegate {
    
    func speechSynthesizer(_ synthesizer: AVSpeechSynthesizer, didFinish utterance: AVSpeechUtterance) {
        self.cameraView.isUserInteractionEnabled = true
        self.spinner.isHidden = true
        self.spinner.stopAnimating()
    }
    
}




