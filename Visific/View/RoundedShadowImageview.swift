//
//  RoundedShadowImageview.swift
//  Visific
//
//  Created by wealthyjalloh on 09/09/2017.
//  Copyright © 2017 CWJ. All rights reserved.
//

import UIKit

class RoundedShadowImageview: UIImageView {

    override func awakeFromNib() {
        self.layer.shadowColor = UIColor.darkGray.cgColor
        self.layer.shadowRadius = 15
        self.layer.shadowOpacity = 0.75
        self.layer.cornerRadius = 15 
        
    }

}
